package crontab

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strings"
)

// Sep is the seperator used in the crontab file, default "\t".
var Sep = "\t"

// Read calls ReadFrom with path "/etc/crontab".
func Read() (*Table, error) {
	return ReadFrom("/etc/crontab")
}

// ReadFrom reads the file at path and parses it as a crontab.
func ReadFrom(path string) (*Table, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("cannot read file at %v: %w", path, err)
	}
	items := make([]Item, 0)
	scanner := bufio.NewScanner(bytes.NewReader(data))
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		parts := strings.Split(line, Sep)
		if len(parts) != 7 {
			continue
		}
		command := parts[6]
		args := ""
		if pos := strings.Index(command, " "); pos > -1 {
			args = command[pos+1:]
			command = command[:pos]
		}
		item := Item{
			Minute:     parts[0],
			Hour:       parts[1],
			DayOfMonth: parts[2],
			Month:      parts[3],
			DayOfWeek:  parts[4],
			User:       parts[5],
			Command:    command,
			Arguments:  args,
		}
		items = append(items, item)
	}
	return &Table{Items: items}, nil
}
