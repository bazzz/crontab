package crontab

import (
	"strings"
	"time"

	"github.com/gorhill/cronexpr"
)

type Item struct {
	Minute     string
	Hour       string
	DayOfMonth string
	Month      string
	DayOfWeek  string
	User       string
	Command    string
	Arguments  string
}

// Concat return a concattenated string of the time elements of this items seperated by a space.
func (i Item) Concat() string {
	return strings.Join([]string{i.Minute, i.Hour, i.DayOfMonth, i.Month, i.DayOfWeek}, " ")
}

// NextRun returns the time this item will run next.
func (i Item) NextRun() time.Time {
	return cronexpr.MustParse(i.Concat()).Next(time.Now())
}
